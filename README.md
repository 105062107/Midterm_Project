# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [ELZa Forum]
* Key functions (add/delete)
    1. [user page]
    2. [post page]
    3. [post list page]
    4. [leave comment under any post]
* Other functions (add/delete)
    1. [登入頁面和創帳號頁面分別]
    2. [創帳號時有重複確認密碼機制，不符的話還有錯誤訊息]
    3. [登入時有錯誤訊息]
    4. [每篇文旁邊都會有頭像，沒有上傳的話會變成預設的]
    5. [可以上傳基本資料與頭像]
    6. [可以查看自己的基本資料和頭像]
    7. [在我的頁面中，擁有所有自己曾經上傳過的貼文]
    8. [可以用其他人的email去找尋其他人的資料]
    9. [可以把自己的資料設成隱私，別人在搜尋的時候就會找不到]


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
    登入頁:一開始可以透過登入頁面下面的按鈕去創帳號()，可以用內部或外部(google)帳號登入
    創帳號頁:直接輸入密碼和按創帳號鍵就可以成功創號
    主論壇頁:每篇文基本都有頭像、帳號和發文內容，要發問要去最下面按submit鍵，最上方可以去主頁、登出
             雖然是在其他頁面發文，可是還是採realtime database，其他人發文會馬上有反應和通知，每篇文都可以點進去
    文頁(post page):可以在指定的那篇文下留言，同樣是有real-time database的功能，只是這裡的繳交不用像主論壇頁中去另一個頁面繳交發文
    發文繳交頁:直接輸入文和submit可以繳交文
    自己主頁:顯示自己曾發過的文，最上方可以去繳交資本資料、基本資料頁面、顯示頭貼去其他人主頁等功能
    提交基本資料頁面:資料輸入完後按資料提交，可以選擇要設成公開或隱私，相片上傳完後按相片上傳，資料和相片是不同的按鈕
    基本資料頁面:會在下面的框框中顯示基本資料，沒有的話會顯示尚未上傳
    找尋其他人資料頁:輸入email會顯示其他人資料，沒有會顯示錯誤，若別人的資料設成隱私也會找不到
    

## Security Report (Optional)
    在firebase中規則中的json中，已經被設定成了需要登入才能取得database的資料，
    接著在主程式中，若user是null，也就是在沒有登入的狀態下，window在onload的時候呼叫的那個init function，
    最一開始就會遇到user是null的錯誤，下面的其他東西也就不會執行
    接著是在找別人資料的頁面，就算有別人的email，但若是在有隱私的狀態下，也沒有辦法看到其他人的資料，這算是一種保護資料的手段
    
    
